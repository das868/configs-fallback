#!/bin/bash
# This script is meant to ease the process of reinstalling my programs by
# outsourcing it to this shell script. I strongly recommend you to write
# one yourself since it’ll save you so much time!

# You can run this script by typing
#     sh fallback-script.sh
# followed by presing <ENTER> into your command line (terminal).

# Availible under the MIT License.
# (c) 2017 das868@Github.com

################################################################################
########################### R U N   A S   R O O T ! ############################
################################################################################

header="################################################################################"

format_nice() {
	clear
	echo "$header"
	echo `expr $1`
	echo "$header"
}

# First, make sure you are in the /home directory
cd ~

# ---The very basic setup---
format_nice "Installing htop and inxi..."
apt-get install -y htop inxi
format_nice "Removing obsolete vim-tiny..."
apt-get remove vim-tiny --purge
#//format_nice "Installing git, tmux, python(3) and dependencies for vim..."
format_nice "Installing git..."
apt-get install -y  git
format_nice "Installing tmux..."
apt-get install -y tmux
format_nice "Installing python(3) and dependencies for vim..."
apt-get install -y libncurses5-dev python python-dev python3 python3-dev
format_nice "Installing latest vim..."
git clone https://github.com/vim/vim
cd vim/src
./configure --with-features=huge \
	--enable-cscope \
	--enable-multibyte \
	--enable-rubyinterp=yes \
	--enable-python3interp=yes \
	--with-python3-config-dir=/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu \
	--enable-perlinterp=yes \
	--enable-luainterp=yes \
	--with-compiledby="das868 <das868@t-online.de>"
make install
format_nice "Please place your .bashrc, .tmux_conf and .vimrc in /home now\nPress <ENTER>
when done..."

format_nice " ****** "
read -n1 -r -p "Press space to continue..." key

# [Routine copied from here: https://goo.gl/gaWJf1 ]
if [ "$key" = '' ]; then
    # Space pressed, do something
    # echo [$key] is empty when SPACE is pressed # uncomment to trace
else
    # Anything else pressed, do whatever else…

#//Install vlc, texlive-full, texstudio, haskell with ghc, geogebra, octave, wxmaxima
format_nice "Installing vlc..."
apt-get install -y vlc
# ---Scientific setup---
format_nice "Installing texlive-full and texstudio..."
apt-get install -y texlive-full texstudio
# Haskell setup
format_nice "Installing Haskell Platform with ghc-7.10.3, ghci-~ and cabal-1.22..."
apt-get update
apt-get install -y software-properties-common
add-apt-repository -y ppa:hvr/ghc
apt-get update
apt-get install -y cabal-install-1.22 ghc-7.10.3
cat >> ~/.bashrc <<EOF
export PATH="\$HOME/.cabal/bin:/opt/cabal/1.22/bin:/opt/ghc/7.10.3/bin:\$PATH"
EOF
export PATH=~/.cabal/bin:/opt/cabal/1.22/bin:/opt/ghc/7.10.3/bin:$PATH

# Mathematical setup
format_nice "Installing GNU Octave (octave)..."
apt-get install -y octave
format_nice "Installing wxMaxima..."
apt-get install -y wxmaxima

# PyQt5 development setup
format_nice "Installing PyQt5 and dependencies (qt5 qt5-dev qt4-designer\n
python3-pyqt5 python3-pyqt5-dbg)..."
apt-get install -y qt5 qt5-dev qt4-designer python3-pyqt5 python3-pyqt5-dbg
### KLAPPT NICHT, STATTDESSEN qt5-default qttools5-dev qttools5-dev-tools
###   pyqt5-dev pyqt5-dev-tools
