"   *******************
"   * VIMRC BY das868 *
"   *******************

" Special thanks for cooperation to github/failipp!

""""""""""""""""""""""""""
" General formatting etc."
""""""""""""""""""""""""""
" enable sytax highlighting
syntax enable
" stop miming your grandfather
set nocompatible
" encoding
set encoding=utf-8
" menu for autocompletion
set wildmenu
" en-fuzzy file search
set path+=**
" search while inputting
set incsearch
" search highlighting
set hlsearch
" ensure correct splitting behavior
set splitright
set splitbelow
" ensure correct behavior of backspace key
set backspace=indent,eol,start
" tab = 4 whitespaces
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" TEMPORARY INSERTED:
set foldlevel=10
set foldmethod=indent
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""
" Plugins & Vundle "
""""""""""""""""""""
" Vundle pre-setup
filetype off
" expand runtime path for Vundle
set rtp+=~/.vim/bundle/Vundle.vim
" actual Vundle setup
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'sjl/badwolf'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'w0rp/ale'
Plugin 'davidhalter/jedi-vim'

call vundle#end()

filetype plugin indent on

""""""""""""""""""""
" Outer appearence "
""""""""""""""""""""
" use colorscheme badwolf
colorscheme badwolf
" display absolute and relative line numbers
set number
set relativenumber
" display ruler at bottom
set ruler
" display entered commands in bottom left corner
set showcmd
" highlight current line and column
" set cursorcolumn
set cursorline
" highlight matched parentheses, only shortly
set showmatch
set matchtime=2

""" FileType au's to be inserted here...

""""""""""""""""""
" (Re-) Mappings "
""""""""""""""""""
" customize leader keys
let mapleader="ö"
let maplocalleader="ä"
" en-lazy usage of colon before commands
nnoremap ü :
" tabedit made simple
nnoremap <leader>te :tabedit<CR>
" folding made simple
set foldmethod=indent
set foldlevel=10
nnoremap <space> za
" tab switching made simple
nnoremap t gt
nnoremap T gT
" saving made simple
nnoremap <leader>w :w<CR>
nnoremap <leader>wa :wa<CR>
" closing made simple
nnoremap <leader>q :q<CR>
nnoremap <leader>qa :qa<CR>
nnoremap <leader>qc :q!<CR>
nnoremap <leader>wq :wq<CR>
" NERDTree made simple
nnoremap <leader>nt :NERDTree<CR>
nnoremap <leader>nc :NERDTreeTabsClose<CR>
" stop overeager hlsearch
nnoremap <leader>h :nohlsearch<CR>
" intuitive pane switching
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l
" only allow scrolling between visual lines
nnoremap j gj
nnoremap k gk
" replace existing line with blank one (expcept @gg/G)
nnoremap do ddko
" send CTRL-A and arrow keys to Nirvana...
nnoremap <c-a> <Nop>
nnoremap <Left> <Nop>
nnoremap <Up> <Nop>
nnoremap <Down> <Nop>
nnoremap <Right> <Nop>

" edit VIMRC with öre
" OLD: nnoremap <leader>re :vsplit $MYVIMRC<CR>
nnoremap <leader>re :tabedit $MYVIMRC<CR>

" reload VIMRC with örr
nnoremap <leader>rr :source $MYVIMRC<CR>

" compile seminar thesis latex and bcf file automatically
nnoremap <leader>c :!./compile-script cola<CR>
"let $datei = :expand('%:t')
"nnoremap <leader>c :!./compile-script $datei 

" quote or parenthese selected text in VISUAL mode
vnoremap <leader>q <esc>`<i"<esc>`>a"<esc>
vnoremap <leader>p <esc>`<i(<esc>`>a)<esc>
vnoremap <leader>p <esc>`<i[<esc>`>a]<esc>
vnoremap <leader>p <esc>`<i{<esc>`>a}<esc>

""""""""""""""""""
" Plugin configs "
""""""""""""""""""
" ---ALE---
" fix code quickly
nnoremap <leader>ale :ALEFix<CR>
" set language-specific fixers
" let g_ale_fixers = {}
let g:ale_fixers = {
			\'python': [
			\   'yapf',
			\   'isort',
			\],
			\}
" show detailed information with warnings and errors with öd
nnoremap <leader>d :ALEDetail<CR>
" ---JEDI-VIM---
" enforce python3 usage
let g:jedi#force_py_version = 3
let g:jedi#popup_on_dot = 0
let g:jedi#popup_select_first = 0
let g_jedi#smart_auto_mappings = 0
let g:jedi#use_splits_not_buffers = 'right'
