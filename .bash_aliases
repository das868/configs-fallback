# Alias declarations
alias alisa='alias'                 #just for fun
alias lg='ls -halF'                 #ls g’scheidt
alias lsc='ls --color=auto'         #ls mit color
alias lgc='ls -half --color=auto'   #ls g’scheidt mit color
alias lcg='lgc'                     #ignore typos in lgc
alias schr='cd ~/Schreibtisch'
alias dow='cd ~/Downloads'
alias mc='make && make clean && clear'
alias mp='cd ~/MeineProjekte'
alias geny='cd ~/Downloads/genymotion && ./genymotion'
alias sem='cd ~/MeineProjekte/LaTeX/Seminararbeit/5.\ Entwurf\ –\ gescheid'
alias gitam='git add . && git commit'
alias gitpu='git push origin master'
alias gits='git status'
alias designer4='/usr/lib/x86_64-linux-gnu/qt4/bin/designer'
alias designer='/usr/lib/x86_64-linux-gnu/qt5/bin/designer'
alias py3='python3'
alias py='python'
alias inxi='inxi -Fx'
alias qtcreator='~/Qt5.9.1/Tools/QtCreator/bin/qtcreator/'
alias qtc='qtcreator'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
