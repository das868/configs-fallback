# configs-fallback
All the config files here will be loaded by the specific programs automatically
from within your /home (~) directory. Here is an overview over the programs and
config files shipped:

| file     | configs for ... included   |
|----------|----------------------------|
|.vimrc    | VIM 8 editor               |
|.bash_aliases   | Linux bash ("Terminal")    |
|.tmux.conf | *T*erminal *Mu*ltiple*x*er |
|fallback-script.sh | List of currently used and favored programs for reinstall |

---

## ~/.vimrc
After having installed the *dependencies* `libncurses5`, `libncurses5-dev` and `python3-dev`, this README.md written on VIM 8.1.290 which was compiled from source using
the following code:    

    git clone https://github.com/vim/vim
    cd vim/src
    ./configure --with-features=huge \
        --enable-cscope \
        --enable-multibyte \
        --enable-rubyinterp=yes \
        --enable-python3interp=yes \
        --with-python3-config-dir=/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu \
        --enable-perlinterp=yes \
        --enable-luainterp=yes \
        --with-compiledby="YOUR NAME HERE"
    sudo make install

## ~./bash_aliases
This file contains some useful alias declarations you might want to use.

## ~./.tmux.conf
Customize your tmux so that color support and mouse mode work!

## fallback-script.sh
Run this script as root from bash to reinstall the most important programs and digital life environment.
I strongly encourage you to write your own one, based on this in case it appeals to you.

---

## Footnote & Disclaimer
Feel free to copy, change and play around with the presented setup. Be aware,
however, that no guarantee is given to you that this setup is going to work for
you the way it did for me. As I am using a linux machine, I cannot offer Apple
or Windows support as well.

This README.md is still in development, as is the entire repository. Therefore expect sudden changes to occur.

Have fun playing around and making use of this stuff!