(define-package "dash" "20220404.2004" "A modern list library for Emacs"
  '((emacs "24"))
  :commit "733abc056ef596c6839355aa5b2ebb85be3ed818" :authors
  '(("Magnar Sveen" . "magnars@gmail.com"))
  :maintainer
  '("Magnar Sveen" . "magnars@gmail.com")
  :keywords
  '("extensions" "lisp")
  :url "https://github.com/magnars/dash.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
