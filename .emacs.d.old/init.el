(require 'package)
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired. See `package-archive-priorities`
;;;; (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;;;; ---------------------------------------------------------------------------------------------
;; Easily switch between open buffers, not having to use C-x b RET:
;(global-set-key (kbd "M-o") 'mode-line-other-buffer)
;(global-set-key (kbd "M-o") 'switch-to-prev-buffer)
;(global-set-key (kbd "M-n") 'switch-to-next-buffer)
;(global-set-key (kbd "M-p") 'switch-to-prev-buffer)
;(global-set-key (kbd "C-M->") 'switch-to-next-buffer)
;(global-set-key (kbd "C-M-<") 'switch-to-prev-buffer)
(global-set-key (kbd "C-ö") 'switch-to-prev-buffer)
(global-set-key (kbd "C-ä") 'switch-to-next-buffer)


;; Resize windows easier than C-x z repeatedly
;(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
;(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
;(global-set-key (kbd "S-C-<down>") 'shrink-window)
;(global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "C->") 'enlarge-window-horizontally)
(global-set-key (kbd "C-<") 'shrink-window-horizontally)

;; Switch windows easier than C-x o
(global-set-key (kbd "C-#") 'other-window)

;; DOESNT WORK: repeat last action completely, just like . in vim
;(global-set-key (kbd "C-.") 'repeat-complex-command)

;; Source: http://www.emacswiki.org/emacs-en/download/misc-cmds.el
(defun revert-buffer-no-confirm ()
    "Revert buffer without confirmation."
    (interactive)
    (revert-buffer :ignore-auto :noconfirm))

;; Easily refresh buffer, e.g. for PDF/LaTeX editing
(global-set-key (kbd "C-M-r") 'revert-buffer-no-confirm)
;;;; ---------------------------------------------------------------------------------------------
(setq inhibit-startup-screen t)
(tool-bar-mode 0) ;remove annoying icon toolbar
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;;;; DOESNT WORK: (set-default-font "DejaVu Sans Mono 14" nil t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector ["white" "#303030" "#b3b3b3" "#606060"])
 '(compilation-message-face 'default)
 '(custom-enabled-themes '(gruber-darker))
 '(custom-safe-themes
   '("7575474658c34b905bcec30a725653b2138c2f2d3deef0587e3abfae08c5b276" "0cd00c17f9c1f408343ac77237efca1e4e335b84406e05221126a6ee7da28971" "5f824cddac6d892099a91c3f612fcf1b09bb6c322923d779216ab2094375c5ee" "16ab866312f1bd47d1304b303145f339eac46bbc8d655c9bfa423b957aa23cc9" "d9646b131c4aa37f01f909fbdd5a9099389518eb68f25277ed19ba99adeb7279" "cab317d0125d7aab145bc7ee03a1e16804d5abdfa2aa8738198ac30dc5f7b569" "39dd7106e6387e0c45dfce8ed44351078f6acd29a345d8b22e7b8e54ac25bac4" "1d78d6d05d98ad5b95205670fe6022d15dabf8d131fe087752cc55df03d88595" "28eb6d962d45df4b2cf8d861a4b5610e5dece44972e61d0604c44c4aad1e8a9d" "8feca8afd3492985094597385f6a36d1f62298d289827aaa0d8a62fe6889b33c" default))
 '(fci-rule-color "#6a737d")
 '(frame-brackground-mode 'dark)
 '(highlight-changes-colors '("#FD5FF0" "#AE81FF"))
 '(highlight-tail-colors
   '(("#3C3D37" . 0)
     ("#679A01" . 20)
     ("#4BBEAE" . 30)
     ("#1DB4D0" . 50)
     ("#9A8F21" . 60)
     ("#A75B00" . 70)
     ("#F309DF" . 85)
     ("#3C3D37" . 100)))
 '(magit-diff-use-overlays nil)
 '(notmuch-search-line-faces
   '(("unread" :foreground "#aeee00")
     ("flagged" :foreground "#0a9dff")
     ("deleted" :foreground "#ff2c4b" :bold t)))
 '(nrepl-message-colors
   '("#032f62" "#6a737d" "#d73a49" "#6a737d" "#005cc5" "#6f42c1" "#d73a49" "#6a737d"))
 '(package-selected-packages
   '(company php-mode smarty-mode auto-complete darkburn-theme darkokai-theme magit markdown-preview-eww markdown-mode haskell-mode gruber-darker-theme rust-mode multiple-cursors badwolf-theme python-mode monochrome-theme monokai-theme paredit smex github-modern-theme github-theme green-is-the-new-black-theme slime slime-repl-ansi-color pdf-view-restore org-bullets org neotree htmlize free-keys))
 '(pdf-view-midnight-colors '("#6a737d" . "#fffbdd"))
 '(pos-tip-background-color "#FFFACE")
 '(pos-tip-foreground-color "#272822")
 '(vc-annotate-background "#3390ff")
 '(vc-annotate-color-map
   '((20 . "#6a737d")
     (40 . "#032f62")
     (60 . "#6a737d")
     (80 . "#6a737d")
     (100 . "#6a737d")
     (120 . "#d73a49")
     (140 . "#6a737d")
     (160 . "#6a737d")
     (180 . "#6a737d")
     (200 . "#6a737d")
     (220 . "#22863a")
     (240 . "#005cc5")
     (260 . "#6f42c1")
     (280 . "#6a737d")
     (300 . "#005cc5")
     (320 . "#6a737d")
     (340 . "#d73a49")
     (360 . "#6a737d")))
 '(vc-annotate-very-old-color "#6a737d")
 '(weechat-color-list
   '(unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#F8F8F2" "#F8F8F0")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "outline" :slant normal :weight normal :height 108 :width normal)))))

(add-to-list 'load-path "~/.emacs.d/elpa/neotree-20200324.1946")
;;(add-to-list 'load-path "~/.emacs.d/elpa/")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

;;(require 'org-superstar)
;;(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(add-to-list 'load-path "/.emacs.d/elpa") ;necessary for emacs to find its own installed packages!
(package-initialize)
;(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;(load-theme 'github-modern-theme t)

;; nicer file path autocompletion because I'm lazy...
(require 'ido)
(ido-mode t)

(require 'smex) ; Not needed if you use package.el
  (smex-initialize) ; Can be omitted. This might cause a (minimal) delay, Smex is auto-initialized on its first run.
;; some smex keybinding
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; set line highlighting parameters
(global-hl-line-mode 0)
;(set-face-background hl-line-face "#fceb00")
(set-face-background 'hl-line "#fceb00") ; Voreinstellung "#3e4446")
(set-face-foreground 'highlight nil) ; really necessary?...

;; inferior lisp mode => paredit mode!
;(defun my-ielm-mode-hook ()
;      (turn-on-eldoc-mode)
;      (and (featurep 'paredit) (paredit-mode 1)))
;(add-hook 'ielm-mode-hook (lambda () (paredit-mode 1))) ; annoying!

;; doesnt work, enables it globally
;(add-hook 'python-mode (load-theme 'badwolf))

;; enable multiple-cursors for editing when needed
(global-set-key (kbd "C-c m c") 'mc/edit-lines)

;; shortcut to execute shell command: no M-!
(global-set-key (kbd "C-ü") 'shell-command)

;; activate org-bullets-mode when org-mode starts
(require 'org-bullets)
(add-hook 'org-mode-hook #'org-bullets-mode)

;; display column numbers too
(setq column-number-mode t)

;; display whitespaces as dots except for org-mode: //UGLY!!!
; (global-whitespace-mode)
;(setq whitespace-mode t)
;(add-hook 'org-mode-hook (lambda () (setq whitespace-mode f)))

;; enable line wrapping by default
(global-visual-line-mode)

;; set utf-8 as default file encoding like under good OS'es
;;  stolen from here: https://stackoverflow.com/questions/1785200/change-emacs-default-coding-system
(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)

;; whitespace highlighting possible via whitespace-mode (ws)

;; map compile command to C-° (= C-S-^)
(global-set-key (kbd "C-°") 'compile)

;; set default font not to DejaVu Sans Mono 14, but Iosevka 14
(set-frame-font "Iosevka 14" nil t)

;; always start company mode upon Emacs startup
(global-company-mode)
