(define-package "transient" "20211023.1125" "Transient commands"
  '((emacs "25.1"))
  :commit "022f20e9581b317bffe3457a2422b3a7075761f6" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("bindings")
  :url "https://github.com/magit/transient")
;; Local Variables:
;; no-byte-compile: t
;; End:
