(define-package "slime" "20211021.807" "Superior Lisp Interaction Mode for Emacs"
  '((cl-lib "0.5")
    (macrostep "0.9"))
  :commit "1378aa718781ec6d359845f0116e12865261b2ca" :keywords
  '("languages" "lisp" "slime")
  :url "https://github.com/slime/slime")
;; Local Variables:
;; no-byte-compile: t
;; End:
