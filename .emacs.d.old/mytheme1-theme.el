(deftheme mytheme1
  "Created 2021-06-28.")

(custom-theme-set-variables
 'mytheme1
 '(custom-safe-themes '("1d78d6d05d98ad5b95205670fe6022d15dabf8d131fe087752cc55df03d88595" "28eb6d962d45df4b2cf8d861a4b5610e5dece44972e61d0604c44c4aad1e8a9d" "8feca8afd3492985094597385f6a36d1f62298d289827aaa0d8a62fe6889b33c" default))
 '(package-selected-packages '(github-modern-theme github-theme green-is-the-new-black-theme slime slime-repl-ansi-color pdf-view-restore org-bullets org neotree htmlize free-keys)))

(custom-theme-set-faces
 'mytheme1
 '(default ((t (:family "DejaVu Sans Mono" :foundry "outline" :slant normal :weight normal :height 108 :width normal)))))

(provide-theme 'mytheme1)
